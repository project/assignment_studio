var myTextExtraction = function (node) {
  //extract data from markup and return it
  if ($(node).hasClass('as_username') == true || $(node).hasClass('as_name') == true || $(node).hasClass('as_group') == true){
    return $(node).html();
  }
  else {
    var output = node.childNodes[0].childNodes[$("#as_view_grade").val()].innerHTML.replace('--%',-1).replace('--','G');
    return output;
  }
}
//extend the drupal js object by adding in an assignment_studio name-space
Drupal.assignment_studio = Drupal.assignment_studio || { functions: {} };

Drupal.assignment_studio.functions.save_feedback = function (button_pressed) {
 if (Drupal.settings.assignment_studio.rubrics) {
  var selected_ary = Array();
  var points_ary = Array();
  var feedback_ary = Array();

  $('.rubric_category_criterion_level_selected').each(function(){
      selected_ary.push($(this).attr('id').replace('rubric_level_',''));
  });
  $('.rubric_grading_earned_input').each(function(){
      if ($(this).hasClass('rubric_grading_input_changed')) {
        points_ary.push($(this).val());
	  }
	  else {
		points_ary.push('@default@');  
	  }
  });
  $('.rubric_grading_feedback_textarea').each(function(){
	  if ($(this).hasClass('rubric_grading_input_changed')) {
        feedback_ary.push(Drupal.assignment_studio.functions.scrub_feedback($(this).val()));
	  }
	  else {
		feedback_ary.push('@default@');  
	  }
  });
  var points = $('.rubric_grading_total_earned_input').val();
  var exemplary = $("#as_exemplary:checkbox:checked").val();
  if (exemplary != 1) {
    exemplary = 0;
  }
  var feedback = Drupal.assignment_studio.functions.scrub_feedback($('#rubric_grading_overall_feedback').val());
  //var save_feedback = $("#as_save_feedback:checkbox:checked").val();
  //if (save_feedback != 1) {
  //  save_feedback = 0;
  //}
  $.ajax({
      type: "POST",
      url: Drupal.settings.basePath + "?q=assignment_studio/ajax/save_rubric_feedback/" + Drupal.settings.assignment_studio.active.nid +"/"+ points +"/"+ exemplary +"/"+ feedback +"/"+ button_pressed +"/"+ selected_ary +"/"+ points_ary +"/"+ feedback_ary,
      success: function(msg){
	    //if (save_feedback == 1) {
	    //  $("#as_save_feedback").click();
	    //  $("#as_saved_feedback").append('<option value="'+ msg +'">'+ $("#as_feedback_title").val() +'</option>');
	    //}
	    if (exemplary == 1) {
	      $("#as_exemplary").click();
        }
      },
	  complete: function(XMLHttpRequest, textStatus) {
		//update display to match back end changes made to points / letter grade
		var percent = Math.round(points / Drupal.settings.assignment_studio.points[Drupal.settings.assignment_studio.active.tid] * 100);
		var letter = '';
		for (var lg in Drupal.settings.assignment_studio.grade_ranges) {
          if(Drupal.settings.assignment_studio.grade_ranges[lg] <= percent && letter == '') {
			letter = lg;
		  }
		}
		$(Drupal.settings.assignment_studio.active.object).children(":first").children(".as_letter_grade").html(letter.toUpperCase().replace('M','-').replace('P','+'));
		$(Drupal.settings.assignment_studio.active.object).children(":first").children(".as_percent_grade").html(percent +'%');
		$(Drupal.settings.assignment_studio.active.object).children(":first").children(".as_number_grade").html(points);
		if ($("#as_view_color").val() == 1) {
		  $(Drupal.settings.assignment_studio.active.object).removeClass($(Drupal.settings.assignment_studio.active.object).attr('grade'));
		  $(Drupal.settings.assignment_studio.active.object).addClass('as_grade_'+ letter);
		}
		$(Drupal.settings.assignment_studio.active.object).attr('grade','as_grade_'+ letter);
		$("#as_view_grade").change();
	  },
    });
  if (button_pressed == 1) {
	  $(Drupal.settings.assignment_studio.active.object).children(":first").removeClass("as_submitted").removeClass("as_graded").addClass("as_ungraded");
    }
    else {
	  $(Drupal.settings.assignment_studio.active.object).children(":first").removeClass("as_submitted").removeClass("as_ungraded").addClass("as_graded");
    }
	//interface may have changed so reload the current window payne
	Drupal.assignment_studio.functions.load_assignment();
 }
 else {
  var save_feedback = $("#as_save_feedback:checkbox:checked").val();
  if (save_feedback != 1) {
    save_feedback = 0;
  }
  var exemplary = $("#as_exemplary:checkbox:checked").val();
  if (exemplary != 1) {
    exemplary = 0;
  }
  var points = $("#as_points").val();
  //trap for bad characters in title
  var title = $("#as_feedback_title").val();
  title = title.replace(/%2F/g,"@2@F@");
  //trap for bad characters in feedback
  var feedback = $("#as_feedback").val();
  feedback = feedback.replace(/%2F/g,"@2@F@");
  var send_data = true;
  if (points == 0 && button_pressed == 0){
	send_data = confirm("Are you sure you want to submit this grade as a 0?");
  }
  if (title == '' && send_data) {
    send_data = confirm("Are you sure you want to submit this assessment without a title?");
  }
  if (feedback == '' && send_data) {
    send_data = confirm("Are you sure you want to submit this assessment without feedback?");
  }
  if (send_data) {
    $.ajax({
      type: "POST",
      url: Drupal.settings.basePath + "?q=assignment_studio/ajax/save_feedback/" + Drupal.settings.assignment_studio.active.nid +"/"+ save_feedback +"/"+ points +"/"+ exemplary +"/"+ title +"/"+ feedback +"/"+ button_pressed,
      success: function(msg){
	    if (save_feedback == 1) {
	      $("#as_save_feedback").click();
	      $("#as_saved_feedback").append('<option value="'+ msg +'">'+ $("#as_feedback_title").val() +'</option>');
	    }
	    if (exemplary == 1) {
	      $("#as_exemplary").click();
        }
      },
	  complete: function(XMLHttpRequest, textStatus) {
		//update display to match back end changes made to points / letter grade
		var percent = Math.round(points / Drupal.settings.assignment_studio.points[Drupal.settings.assignment_studio.active.tid] * 100);
		var letter = '';
		for (var lg in Drupal.settings.assignment_studio.grade_ranges) {
          if(Drupal.settings.assignment_studio.grade_ranges[lg] <= percent && letter == '') {
			letter = lg;
		  }
		}
		$(Drupal.settings.assignment_studio.active.object).children(":first").children(".as_letter_grade").html(letter.toUpperCase().replace('M','-').replace('P','+'));
		$(Drupal.settings.assignment_studio.active.object).children(":first").children(".as_percent_grade").html(percent +'%');
		$(Drupal.settings.assignment_studio.active.object).children(":first").children(".as_number_grade").html(points);
		if ($("#as_view_color").val() == 1) {
		  $(Drupal.settings.assignment_studio.active.object).removeClass($(Drupal.settings.assignment_studio.active.object).attr('grade'));
		  $(Drupal.settings.assignment_studio.active.object).addClass('as_grade_'+ letter);
		}
		$(Drupal.settings.assignment_studio.active.object).attr('grade','as_grade_'+ letter);
		$("#as_view_grade").change();
	  },
    });
    if (button_pressed == 1) {
	  $(Drupal.settings.assignment_studio.active.object).children(":first").removeClass("as_submitted").addClass("as_ungraded");
    }
    else {
	  $(Drupal.settings.assignment_studio.active.object).children(":first").removeClass("as_submitted").removeClass("as_ungraded").addClass("as_graded");
    }
	//interface may have changed so reload the current window payne
	Drupal.assignment_studio.functions.load_assignment();
  }
 }
};

Drupal.assignment_studio.functions.scrub_feedback = function(feedback) {
	var tmpfeedback = feedback.replace(/,/g,'@comma@');
	tmpfeedback = tmpfeedback.replace(/#/g,'@pound@');
	tmpfeedback = tmpfeedback.replace(/&/g,'@amp@');
	
	var tmp = tmpfeedback.split('+');
	tmpfeedback = tmp.join('@plus@');
	
	tmp = tmpfeedback.split('/');
	tmpfeedback = tmp.join('@fwdslash@');
	
	tmp = tmpfeedback.split('\\');
	tmpfeedback = tmp.join('@bckslash@');
	return tmpfeedback;
};

Drupal.assignment_studio.functions.loading = function ($obj,title) {
  if (title == '') {
    $obj.html('<div class="as_loading" style="background-image:url('+ Drupal.settings.basePath +'misc/throbber.gif);">'+ $obj.html() +'</div>');
  }
  else {
    $obj.html(title);
  }
};

Drupal.assignment_studio.functions.toggle_colorization = function (toggle) {
  if (toggle == 'show') {
    $('.as_assignment').each(function() {
      $(this).addClass($(this).attr('grade'));
    });
  }
  else {
    $('.as_assignment').each(function() {
      $(this).removeClass($(this).attr('grade'));
    });
  }
};

Drupal.assignment_studio.functions.refresh_view_type = function (){
  switch($('#as_view_grade').val()) {
  case '0':
	$('.as_letter_grade, .as_percent_grade, .as_number_grade').css('display','');
  break;
  case '1':
	$('.as_letter_grade').css('display','none');
	$('.as_percent_grade').css('display','block');
	$('.as_number_grade').css('display','');
  break;
  case '2':
	$('.as_letter_grade').css('display','none');
	$('.as_percent_grade').css('display','');
	$('.as_number_grade').css('display','block');
  break;
  case '3':
	$('.as_letter_grade').css('display','none');
	$('.as_percent_grade').css('display','none');
	$('.as_number_grade').css('display','none');
  break;
  }	
};

Drupal.assignment_studio.functions.pagechange_click = function () {
  var str = 'first';
  if ($(this).hasClass('last') == true) {
    str = 'last';
  }
  $("#assignment_table td").removeClass('as_active_assignment');
  Drupal.settings.assignment_studio.tmp_obj = $("#assignment_table").find(".as_col_1:"+ str);
  Drupal.settings.assignment_studio.previous.object = Drupal.settings.assignment_studio.active.object;
  Drupal.settings.assignment_studio.active.object = Drupal.settings.assignment_studio.tmp_obj;
  $(Drupal.settings.assignment_studio.active.object).css('background-color','');
  $(Drupal.settings.assignment_studio.active.object).addClass('as_active_assignment');
};

Drupal.assignment_studio.functions.load_assignment = function () {
  //display the node that's been activated
  $("#as_saved_feedback").val(0);
  $.ajax({
	type: "POST",
	url: Drupal.settings.basePath + "?q=assignment_studio/ajax/display/" + Drupal.settings.assignment_studio.active.nid,
	beforeSend: function (){
      Drupal.assignment_studio.functions.loading($("#as_assignment_button"),'');
	},
	success: function(msg){
	  $("#as_assignment_window_1").html(msg);
	  $("#as_assignment_window_1").prepend("</h2><h2>Due: "+ Drupal.settings.assignment_studio.dates[Drupal.settings.assignment_studio.active.tid] +'</h2><a href="'+ Drupal.settings.basePath +'?q=node/'+ Drupal.settings.assignment_studio.active.nid +'" target="_blank">Click to open assignment in new window</a>');
	  if (Drupal.settings.assignment_studio.submission_locations[Drupal.settings.assignment_studio.active.tid] == 0) {
      $("#as_assignment_window_1").prepend(" (submitted external to studio)");
    }
	  $("#as_assignment_window_1").prepend("<h2>"+ Drupal.settings.assignment_studio.assignments[Drupal.settings.assignment_studio.active.tid]);
      Drupal.assignment_studio.functions.loading($("#as_assignment_button"),'Assignment');
	},
  });
};
//get past comments if any exists
Drupal.assignment_studio.functions.load_comments = function () {
  $.ajax({
	type: "POST",
	url: Drupal.settings.basePath + "?q=assignment_studio/ajax/get_comments/" + Drupal.settings.assignment_studio.active.nid,
	beforeSend: function (){
		Drupal.assignment_studio.functions.loading($("#as_comments_button"),'');
	},
	success: function(msg){
	  $("#as_comments_window_1").html(msg);
	  Drupal.assignment_studio.functions.loading($("#as_comments_button"),'Comments');
	},
  });	
};
Drupal.assignment_studio.functions.load_student = function () {
  Drupal.assignment_studio.functions.loading($("#as_student_button"),'');
  $("#as_student_window_1").html('<h2>'+ $(Drupal.settings.assignment_studio.active.object).siblings(":first").html() +'</h2>');
  //get stuent profile info
  $.ajax({
    type: "POST",
    url: Drupal.settings.basePath + "?q=assignment_studio/ajax/student/" + Drupal.settings.assignment_studio.active.uid,
    success: function(msg){
      $("#as_student_window_1").append(msg);
      Drupal.assignment_studio.functions.loading($("#as_student_button"),"Student");
    },
  });
};
Drupal.assignment_studio.functions.load_feedback = function () {
  if ($(Drupal.settings.assignment_studio.active.object).children(':first').hasClass('as_ungraded') || $(Drupal.settings.assignment_studio.active.object).children(':first').hasClass('as_graded') || Drupal.settings.assignment_studio.rubrics) {
    //clear previous rubric in the transition; cleaner to understand it's loading
	$("#as_feedback_window_1").html('');
	$.ajax({
	    type: "POST",
	    url: Drupal.settings.basePath + "?q=assignment_studio/ajax/get_feedback/"+ Drupal.settings.assignment_studio.active.nid,
		beforeSend:function (){
		  Drupal.assignment_studio.functions.loading($("#as_feedback_button"),'');
		},
	    success: function(msg){
		  if (!Drupal.settings.assignment_studio.rubrics) {
		    var msg_ary = msg.split('~@~@~@~',3);
		    $("#as_points").html('');
            for (var i=0; i<=Drupal.settings.assignment_studio.points[Drupal.settings.assignment_studio.active.tid]; i++) {
              $("#as_points").append('<option value="'+ i +'">'+ i +'</option>');
            }
		    $("#as_points").val(msg_ary[0]);
		    $("#as_feedback_title").val(msg_ary[1]);
		    $("#as_feedback").val(msg_ary[2]);
		    Drupal.assignment_studio.functions.loading($("#as_feedback_button"),'Feedback');
		  }
		  else {
            $("#as_feedback_window_1").html(msg);
			Drupal.assignment_studio.functions.loading($("#as_feedback_button"),'Feedback');
			$('#as_feedback_button').click();
			equalHeight($(".rubric_category_criterion_level, .rubric_category_criterion_header"));
		  }
		},
    });
  }	
};

Drupal.assignment_studio.functions.widget = function () {
  $(".as_up").click(function(){
	if ($(Drupal.settings.assignment_studio.active.object).parent().prev().attr('id') != null) {
	  $(Drupal.settings.assignment_studio.active.object).removeClass('as_active_assignment');
	  for(var i=1; i<Drupal.settings.assignment_studio.assignment_count+1; i++) {
	    if($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_'+ i)) {
	      Drupal.settings.assignment_studio.tmp_obj = $(Drupal.settings.assignment_studio.active.object).parent().prev().find(".as_col_"+i);
		  Drupal.settings.assignment_studio.previous.object = Drupal.settings.assignment_studio.active.object;
		  Drupal.settings.assignment_studio.active.object = Drupal.settings.assignment_studio.tmp_obj;
		  $(Drupal.settings.assignment_studio.active.object).css('background-color','');
		  $(Drupal.settings.assignment_studio.active.object).addClass('as_active_assignment');
        }
	  }
	}
	else {
      if (config.page != 0) {
	    for(var i=1; i<Drupal.settings.assignment_studio.assignment_count+1; i++) {
	      if($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_'+ i)) {
	        $(".prev").click();
			$("#assignment_table td").removeClass('as_active_assignment');
		    Drupal.settings.assignment_studio.tmp_obj = $("#assignment_table").find(".as_col_"+ i +":last");
		    Drupal.settings.assignment_studio.previous.object = Drupal.settings.assignment_studio.active.object;
			Drupal.settings.assignment_studio.active.object = Drupal.settings.assignment_studio.tmp_obj;
		    $(Drupal.settings.assignment_studio.active.object).css('background-color','');
		    $(Drupal.settings.assignment_studio.active.object).addClass('as_active_assignment');
          }
	    }
	  }	
	}
  });
  $(".as_up").hover(function(){
	if ($(Drupal.settings.assignment_studio.active.object).parent().prev().attr('id') != null) {
	  for(var i=1; i<Drupal.settings.assignment_studio.assignment_count+1; i++) {
	    if($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_'+ i)) {
	      Drupal.settings.assignment_studio.tmp_obj = $(Drupal.settings.assignment_studio.active.object).parent().prev().find(".as_col_"+i);
		  $(Drupal.settings.assignment_studio.tmp_obj).css('background-color','#FF6666');
        }
	  }
	}
  },function(){
	if ($(Drupal.settings.assignment_studio.active.object).parent().prev().attr('id') != null) {
      for(var i=1; i<Drupal.settings.assignment_studio.assignment_count+1; i++) {
	    if($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_'+ i)) {
	      Drupal.settings.assignment_studio.tmp_obj = $(Drupal.settings.assignment_studio.active.object).parent().prev().find(".as_col_"+i);
		  $(Drupal.settings.assignment_studio.tmp_obj).css('background-color','');
        }
	  }
	}
  });
  //allow for quick grade of next student
  $(".as_down").click(function(){
	if ($(Drupal.settings.assignment_studio.active.object).parent().next().attr('id') != null) {		
	  $(Drupal.settings.assignment_studio.active.object).removeClass('as_active_assignment');
	  for(var i=1; i<Drupal.settings.assignment_studio.assignment_count+1; i++) {
	    if($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_'+ i)) {
	      Drupal.settings.assignment_studio.tmp_obj = $(Drupal.settings.assignment_studio.active.object).parent().next().find(".as_col_"+i);
		  Drupal.settings.assignment_studio.previous.object = Drupal.settings.assignment_studio.active.object;
		  Drupal.settings.assignment_studio.active.object = Drupal.settings.assignment_studio.tmp_obj;
		  $(Drupal.settings.assignment_studio.active.object).css('background-color','');
		  $(Drupal.settings.assignment_studio.active.object).addClass('as_active_assignment');
        }
	  }
	}
	else {
	  var pages = config.totalRows / $('.pagesize').val();
	  if (parseInt(config.totalRows) % parseInt($('.pagesize').val()) == 0) {
	    pages = Math.round(pages);
	  }
      else {
	    pages = Math.round(pages) + 1;
	  }
      if ((config.page+1) != pages) {
	    for(var i=1; i<Drupal.settings.assignment_studio.assignment_count+1; i++) {
	      if($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_'+ i)) {
	        $(".next").click();
			$("#assignment_table td").removeClass('as_active_assignment');
		    Drupal.settings.assignment_studio.tmp_obj = $("#assignment_table").find(".as_col_"+ i +":first");
		    Drupal.settings.assignment_studio.previous.object = Drupal.settings.assignment_studio.active.object;
			Drupal.settings.assignment_studio.active.object = Drupal.settings.assignment_studio.tmp_obj;
		    $(Drupal.settings.assignment_studio.active.object).css('background-color','');
		    $(Drupal.settings.assignment_studio.active.object).addClass('as_active_assignment');
          }
	    }
	  }
	}
  });
  $(".as_down").hover(function(){
	if ($(Drupal.settings.assignment_studio.active.object).parent().next().attr('id') != null) {
	  for(var i=1; i<Drupal.settings.assignment_studio.assignment_count+1; i++) {
	    if($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_'+ i)) {
	      Drupal.settings.assignment_studio.tmp_obj = $(Drupal.settings.assignment_studio.active.object).parent().next().find(".as_col_"+i);
		  $(Drupal.settings.assignment_studio.tmp_obj).css('background-color','#FF6666');
        }
	  }
	}
  },function(){
    if ($(Drupal.settings.assignment_studio.active.object).parent().next().attr('id') != null) {
	  for(var i=1; i<Drupal.settings.assignment_studio.assignment_count+1; i++) {
	    if($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_'+ i)) {
	      Drupal.settings.assignment_studio.tmp_obj = $(Drupal.settings.assignment_studio.active.object).parent().next().find(".as_col_"+i);
		  $(Drupal.settings.assignment_studio.tmp_obj).css('background-color','');
        }
	  }
	}
  });
  //allow for quick grade of next assignment
  $(".as_right").click(function(){
    if($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_'+ Drupal.settings.assignment_studio.assignment_count) == false) {
      $(Drupal.settings.assignment_studio.active.object).removeClass('as_active_assignment');
	  Drupal.settings.assignment_studio.previous.object = Drupal.settings.assignment_studio.active.object;
	  Drupal.settings.assignment_studio.active.object = $(Drupal.settings.assignment_studio.active.object).next();
	  $(Drupal.settings.assignment_studio.active.object).css('background-color','');
	  $(Drupal.settings.assignment_studio.active.object).addClass('as_active_assignment');
	}
  });
  $(".as_right").hover(function(){
	if($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_'+ Drupal.settings.assignment_studio.assignment_count) == false) {
      $(Drupal.settings.assignment_studio.active.object).next().css('background-color','#FF6666');
	}
  },function(){
    $(Drupal.settings.assignment_studio.active.object).next().css('background-color','');
  });
  //allow for quick grade of previous assignment
  $(".as_left").click(function(){
	if ($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_1') == false) {
      $(Drupal.settings.assignment_studio.active.object).removeClass('as_active_assignment');
	  Drupal.settings.assignment_studio.previous.object = Drupal.settings.assignment_studio.active.object;
	  Drupal.settings.assignment_studio.active.object = $(Drupal.settings.assignment_studio.active.object).prev();
	  $(Drupal.settings.assignment_studio.active.object).css('background-color','');
	  $(Drupal.settings.assignment_studio.active.object).addClass('as_active_assignment');
	}
  });
  $(".as_left").hover(function(){
	if ($(Drupal.settings.assignment_studio.active.object).hasClass('as_col_1') == false) {
      $(Drupal.settings.assignment_studio.active.object).prev().css('background-color','#FF6666');
	}
  },function(){
    $(Drupal.settings.assignment_studio.active.object).prev().css('background-color','');
  });
  //allow quick opening of previous item
  $(".as_go").click(function(){
	$(Drupal.settings.assignment_studio.active.object).click();
  });
  //display / hide widget menus
  $(".as_widget_button").click(function(){
	$(".as_widget_tab").css('display','');
	$("#"+ this.id +"_tab").css('display','block');
  });	
};
