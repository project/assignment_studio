var warning = true;
  window.onbeforeunload = function() { 
    if (warning) {
      return 'You will have to reload the assignment studio if you leave this page, are you sure you want to?';
    }
  }
  
$(document).ready(function(){

  $("#as_filter_group").change(function(){
	var page_size = $('.pagesize').val();
	var curr_page = config.page;
	$("#assignment_table").tablesorterPager({size: config.totalRows});	
	if($(this).val() == '') {
      $('.as_group').parent().css('display','');
	  $(".pagesize, .first, .prev, .last, .next").unbind('click');
	$("#assignment_table").tablesorterPager({size: page_size, page: curr_page});
	$(".pagesize, .first, .prev, .last, .next").click(Drupal.assignment_studio.functions.pagechange_click);
	}
	else {
      $('.as_group').parent().css('display','none');
      $('.as_group_'+ $(this).val()).parent().css('display','');
	  $(".pagesize, .first, .prev, .last, .next").unbind('click');
	  $("#assignment_table").tablesorterPager({size: config.totalRows, page: curr_page});
	  $(".pagesize, .first, .prev, .last, .next").click(Drupal.assignment_studio.functions.pagechange_click);
	}
  });
  $("#as_filter_student").change(function(){
    //need to do some selection in visible pages to find visible record
    var page_size = $('.pagesize').val();
	var curr_page = config.page;
	$("#assignment_table").tablesorterPager({size: config.totalRows});	
	if($(this).val() == '') {
      $('.as_col_1').parent().css('display','');
	  $(".pagesize, .first, .prev, .last, .next").unbind('click');
	$("#assignment_table").tablesorterPager({size: page_size, page: curr_page});
	$(".pagesize, .first, .prev, .last, .next").click(Drupal.assignment_studio.functions.pagechange_click);
	}
	else {
      $('.as_col_1').parent().css('display','none');
      $('#assignment_'+ $(this).val()).css('display','');
	  $(".pagesize, .first, .prev, .last, .next").unbind('click');
	$("#assignment_table").tablesorterPager({size: config.totalRows, page: curr_page});
	$(".pagesize, .first, .prev, .last, .next").click(Drupal.assignment_studio.functions.pagechange_click);
	}
  });
  $("#as_filter_assignment").change(function(){
    var page_size = $('.pagesize').val();
	var curr_page = config.page;
	$("#assignment_table").tablesorterPager({size: config.totalRows});	
	if($(this).val() == '') {
      for(var i=1; i<Drupal.settings.assignment_studio.assignment_count+1; i++) {
        $('.as_col_' + i +',.as_col_head_'+ i).css('display','');
	  }
	}
	else {
      for(var i=1; i<Drupal.settings.assignment_studio.assignment_count+1; i++) {
        if ($(this).val() == i) {
		  $('.as_col_' + i +',.as_col_head_'+ i).css('display','');
	    }
	    else {
          $('.as_col_' + i +',.as_col_head_'+ i).css('display','none');
	    }
	  }
	}
	$(".pagesize, .first, .prev, .last, .next").unbind('click');
	if ($("#as_filter_group").val() != '' || $("#as_filter_student").val() != '') { 
	  $("#assignment_table").tablesorterPager({size: config.totalRows, page: curr_page});
	}
	else {
	  $("#assignment_table").tablesorterPager({size: page_size, page: curr_page});  
	}
	$(".pagesize, .first, .prev, .last, .next").click(Drupal.assignment_studio.functions.pagechange_click);
  });
  $("#as_filter_status").change(function(){
	var page_size = $('.pagesize').val();
	var curr_page = config.page;
	$("#assignment_table").tablesorterPager({size: config.totalRows});					
    switch($(this).val()) {
	  case 'submitted':
	    $('.as_submitted').css('display','');
		$('.as_unsubmitted').css('display','none');
		$('.as_graded').css('display','none');
		$('.as_ungraded').css('display','none');
	  break;
	  case 'ungraded':
	    $('.as_submitted').css('display','none');
		$('.as_unsubmitted').css('display','none');
		$('.as_graded').css('display','none');
		$('.as_ungraded').css('display','');
	  break;
	  case 'graded':
	    $('.as_submitted').css('display','none');
		$('.as_unsubmitted').css('display','none');
		$('.as_graded').css('display','');
		$('.as_ungraded').css('display','none');
	  break;
	  case 'unsubmitted':
	    $('.as_submitted').css('display','none');
		$('.as_unsubmitted').css('display','');
		$('.as_graded').css('display','none');
		$('.as_ungraded').css('display','none');
	  break;
	  default:
	    $('.as_submitted').css('display','');
		$('.as_unsubmitted').css('display','');
		$('.as_graded').css('display','');
		$('.as_ungraded').css('display','');
	  break;
	}
	$(".pagesize, .first, .prev, .last, .next").unbind('click');
	if ($("#as_filter_group").val() != '' || $("#as_filter_student").val() != '') { 
	  $("#assignment_table").tablesorterPager({size: config.totalRows, page: curr_page});
	}
	else {
	  $("#assignment_table").tablesorterPager({size: page_size, page: curr_page});  
	}
	$(".pagesize, .first, .prev, .last, .next").click(Drupal.assignment_studio.functions.pagechange_click);
  });
  $(".as_top_button").click(function(){
	$('.as_window_tab').css('display','block');
    $(".as_top_button").removeClass("as_top_button_active");
	$(this).addClass("as_top_button_active");
	if ($("#as_collapse").hasClass("as_collapse_active") == true) {
	  $("#as_collapse").removeClass("as_collapse_active");
	  $("#as_overview").css('height',Drupal.settings.assignment_studio.overview_height);
	  $("#as_windows").removeClass('as_hide_windows');
	}
	$('.as_overview_window').removeClass('as_overview_window_active');
	var tmp_id = $(this).attr('id').replace('_button','_window_');
	$('#'+ tmp_id +'1').addClass('as_overview_window_active');
	//$('#'+ tmp_id +'1').parent().css('display','block');
	if ($("#as_split_window").hasClass('as_split_active') == true) {
	  $('#'+ tmp_id +'2').addClass('as_overview_window_active');
	  //$('#'+ tmp_id +'2').parent().css('display','block');
	}
	//$('.scroll-pane').jScrollPane();
  });
  $("#as_split_window").click(function(){
    $(this).toggleClass("as_split_active");
	var tmp_id = $('.as_overview_window_active:first').attr('id').replace('1','2');
	if ($(this).hasClass("as_split_active") == true) {
	  $(".as_overview_window:even").addClass('as_overview_window_split_left');
	  $(".as_overview_window:odd").addClass('as_overview_window_split_right');
	  $("#as_window_tab_1").addClass('as_overview_window_split_left');
	  $("#as_window_tab_2").addClass('as_overview_window_split_right');
	  $('#'+ tmp_id).addClass('as_overview_window_active');
	}
	else {
	  $(".as_overview_window").removeClass('as_overview_window_split_left').removeClass('as_overview_window_split_right');
	  $("#as_window_tab_1").removeClass('as_overview_window_split_left');
	  $("#as_window_tab_2").removeClass('as_overview_window_split_right');
	  $('#'+ tmp_id).removeClass('as_overview_window_active');
	}
  });
  
  $("#as_collapse").click(function(){
    $(this).toggleClass("as_collapse_active");
	if ($(this).hasClass("as_collapse_active") == true) {
      $("#as_overview").css('height','40px');
	  $("#as_windows").addClass('as_hide_windows');
	}
	else {
	  $("#as_overview").css('height',Drupal.settings.assignment_studio.overview_height);
	  $("#as_windows").removeClass('as_hide_windows');
	}
  });
  
  $("#assignment_table").tablesorter({
    widgets: ['zebra'],
    textExtraction: myTextExtraction,
	debug: false,
	widthFixed:true,
  }).bind("sortStart",function() {
    //start sorting process
  }).bind("sortEnd",function() {
    //end sorting process
  }).tablesorterPager({container: $("#tablesorterPager")});
  $(".pagesize, .first, .prev, .last, .next").click(Drupal.assignment_studio.functions.pagechange_click);
  for (var i in Drupal.settings.assignment_studio.assignments) {
    Drupal.settings.assignment_studio.assignment_count++;	
  }
  $("#as_view_exemplary").change(function(){
	var page_size = $('.pagesize').val();
	var curr_page = config.page;
	$("#assignment_table").tablesorterPager({size: config.totalRows});					  
    if ($(this).val() == 1) {
	  $(".as_exemplary").css('display','block');
	}
	else {
	  $(".as_exemplary").css('display','');
    }
	$(".pagesize, .first, .prev, .last, .next").unbind('click');
	if ($("#as_filter_group").val() != '' || $("#as_filter_student").val() != '') { 
	  $("#assignment_table").tablesorterPager({size: config.totalRows, page: curr_page});
	}
	else {
	  $("#assignment_table").tablesorterPager({size: page_size, page: curr_page});  
	}
	$(".pagesize, .first, .prev, .last, .next").click(Drupal.assignment_studio.functions.pagechange_click);
  });
  $("#as_view_grade").change(function(){
    var page_size = $('.pagesize').val();
	var curr_page = config.page;
	$("#assignment_table").tablesorterPager({size: config.totalRows});
	Drupal.assignment_studio.functions.refresh_view_type();
	$("#assignment_table").trigger("update");
	$("#assignment_table").trigger("appendCache");
	$(".pagesize, .first, .prev, .last, .next").unbind('click');
	if ($("#as_filter_group").val() != '' || $("#as_filter_student").val() != '') { 
	  $("#assignment_table").tablesorterPager({size: config.totalRows, page: curr_page});
	}
	else {
	  $("#assignment_table").tablesorterPager({size: page_size, page: curr_page});  
	}
	$(".pagesize, .first, .prev, .last, .next").click(Drupal.assignment_studio.functions.pagechange_click);
  });
  $("#as_view_color").change(function(){
	var page_size = $('.pagesize').val();
	var curr_page = config.page;
	$("#assignment_table").tablesorterPager({size: config.totalRows});
    if ($(this).val() == 1) {
      Drupal.assignment_studio.functions.toggle_colorization('show');
	}
	else {
	  Drupal.assignment_studio.functions.toggle_colorization('hide');	
	}
	$(".pagesize, .first, .prev, .last, .next").unbind('click');
	if ($("#as_filter_group").val() != '' || $("#as_filter_student").val() != '') { 
	  $("#assignment_table").tablesorterPager({size: config.totalRows, page: curr_page});
	}
	else {
	  $("#assignment_table").tablesorterPager({size: page_size, page: curr_page});  
	}
	$(".pagesize, .first, .prev, .last, .next").click(Drupal.assignment_studio.functions.pagechange_click);
  });
  $("#as_control_widget").draggable({
    handle: '.as_drag',
	cursor: 'move',
	stop: function(event,ui) {
	  if (parseInt($(this).css('left')) < 0) {
	    $(this).css('left','0px');
	  }
	  else if ( (parseInt($(this).css('left'))+parseInt($(this).css('width'))) > $(window).width()) {
		$(this).css('left',$(window).width() - parseInt($(this).css('width')) +'px');  
	  }
	  if (parseInt($(this).css('top')) < 0) {
        $(this).css('top','0px');
	  }
	  else if ( (parseInt($(this).css('top'))+parseInt($(this).css('height'))) > $(window).height()) {
		$(this).css('top',$(window).height()-parseInt($(this).css('height')) +'px');  
	  }
	},
  });
  $("#as_overview").resizable({
    handles: 's,se,e',
	//grid: [10,10],
	cursor: 'move',
	//alsoResize: '.as_overview_window',
	minHeight: 40,
	maxHeight: 600,
	minWidth: 720,
	resize: function() {
	  if (parseInt($("#as_overview").css('height')) <= 80) {
		$("#as_windows").addClass('as_hide_windows');
	  }
	  else {
		$("#as_windows").removeClass('as_hide_windows');
	  }
	},
	stop: function(event,ui) {
	  if ( (parseInt($(this).css('left'))+parseInt($(this).css('width'))) > $(window).width()) {
        $(this).css('width',($(window).width() - parseInt($(this).css('left'))) +'px');
	  }
	  if ( (parseInt($(this).css('top'))+parseInt($(this).css('height'))) > $(window).height()) {
		$(this).css('height',$(window).height()-parseInt($(this).css('top')) +'px');  
	  }
	  if (parseInt($("#as_overview").css('height')) == 40) {
		$("#as_collapse").addClass("as_collapse_active");
		Drupal.settings.assignment_studio.overview_height = '250px';
		$("#as_windows").addClass('as_hide_windows');
	  }
	  else {
		$("#as_collapse").removeClass("as_collapse_active");
		Drupal.settings.assignment_studio.overview_height = $(this).css('height');
		$("#as_windows").removeClass('as_hide_windows');
	  }
	}
  });/*.draggable({
	handle: '#as_move_button',
	cursor: 'move',
	cursorAt: {top:5,left:5},
	axis: 'x',
	grid: [10,10],
	stop: function(event,ui) {
	   if (parseInt($(this).css('left')) < 0) {
	    $(this).css('left','0px');
	  }
	  else if ( (parseInt($(this).css('left'))+parseInt($(this).css('width'))) > $(window).width()) {
		$(this).css('left',Math.round(($(window).width() - parseInt($(this).css('width')))/10)*10 +'px');  
	  }
	  if (parseInt($(this).css('top')) < 0) {
        $(this).css('top','0px');
	  }
	  else if ( (parseInt($(this).css('top'))+parseInt($(this).css('height'))) > $(window).height()) {
		$(this).css('top',$(window).height()-parseInt($(this).css('height')) +'px');  
	  }
	},
  });*/
  
  //set up the initial top left object
  Drupal.settings.assignment_studio.active.object = $(".as_col_1:first");
  Drupal.settings.assignment_studio.previous.object = Drupal.settings.assignment_studio.active.object;
  $(Drupal.settings.assignment_studio.active.object).addClass('as_active_assignment');
  var tmp_ary = $(Drupal.settings.assignment_studio.active.object).attr('id').split('_');
  Drupal.settings.assignment_studio.active.uid = tmp_ary[1];
  Drupal.settings.assignment_studio.active.tid = tmp_ary[2];
  Drupal.settings.assignment_studio.active.nid = tmp_ary[3];
  Drupal.settings.assignment_studio.previous.uid = Drupal.settings.assignment_studio.active.uid;
  Drupal.settings.assignment_studio.previous.tid = Drupal.settings.assignment_studio.active.tid;
  Drupal.settings.assignment_studio.previous.nid = Drupal.settings.assignment_studio.active.nid;
  //setup remote control listeners
  Drupal.assignment_studio.functions.widget();
  
  $(".as_active_assignment").hover(function(){
    $(Drupal.settings.assignment_studio.active.object).css('background-color','#FF6666');
  },function(){
    $(Drupal.settings.assignment_studio.active.object).css('background-color','');
  });
  
  $(".as_assignment").live('mouseover',function(){
    $(this).css('background-color','#FFFFAA');
  }).live('mouseout',function(){
    $(this).css('background-color','');
  });
  //UI stuff to select and handle ALL assignment clicks
  $(".as_assignment").live('click',function(){
    $(Drupal.settings.assignment_studio.active.object).removeClass('as_active_assignment');
    Drupal.settings.assignment_studio.previous.object = Drupal.settings.assignment_studio.active.object;
	Drupal.settings.assignment_studio.active.object = this;
    var ary = $(Drupal.settings.assignment_studio.active.object).attr('id').split('_');
    Drupal.settings.assignment_studio.previous.uid = Drupal.settings.assignment_studio.active.uid;
	Drupal.settings.assignment_studio.previous.tid = Drupal.settings.assignment_studio.active.tid;
	Drupal.settings.assignment_studio.previous.nid = Drupal.settings.assignment_studio.active.nid;
	Drupal.settings.assignment_studio.active.uid = ary[1];
    Drupal.settings.assignment_studio.active.tid = ary[2];
    Drupal.settings.assignment_studio.active.nid = ary[3];
	//pass html content over to window_2 version
	$("#as_student_window_2").html($("#as_student_window_1").html());
	$("#as_assignment_window_2").html($("#as_assignment_window_1").html());
	$("#as_comments_window_2").html($("#as_comments_window_1").html());
	if (Drupal.settings.assignment_studio.rubrics == false) {
	  $("#as_feedback_window_2").html($("#as_feedback_window_1").html());
	}
	$("#as_additional_window_2").html($("#as_additional_window_1").html());
	$("#as_window_tab_2").html($("#as_window_tab_1").html());
	
	$(Drupal.settings.assignment_studio.active.object).addClass('as_active_assignment');
    if ($(Drupal.settings.assignment_studio.active.object).children(':first').hasClass('as_graded')) {
      $(".as_save").css('display','none');
	}
	else {
      $(".as_save").css('display','');
	}
	Drupal.assignment_studio.functions.load_student();
	//status bar stuff, this needs to be placed somewhere in the top bar
	$("#as_window_tab_1").html(Drupal.settings.assignment_studio.assignments[Drupal.settings.assignment_studio.active.tid]);
    
	if (Drupal.settings.assignment_studio.active.nid != 0 || Drupal.settings.assignment_studio.submission_locations[Drupal.settings.assignment_studio.active.tid] == 0) {
	  if (Drupal.settings.assignment_studio.submission_locations[Drupal.settings.assignment_studio.active.tid] == 0 && Drupal.settings.assignment_studio.active.nid == 0) {
        $.ajax({
		  type: "POST",
	      url: Drupal.settings.basePath + "?q=assignment_studio/ajax/create_null/" + Drupal.settings.assignment_studio.active.tid +"/"+ Drupal.settings.assignment_studio.active.uid,
		  success: function(msg) {
			Drupal.settings.assignment_studio.active.nid = msg;
		    $(Drupal.settings.assignment_studio.active.object).attr('id','assignment_'+ Drupal.settings.assignment_studio.active.uid +'_'+ Drupal.settings.assignment_studio.active.tid +'_'+ Drupal.settings.assignment_studio.active.nid);
		  },
		  complete: function(XMLHttpRequest, textStatus) {
		    $(Drupal.settings.assignment_studio.active.object).children(":first").removeClass("as_unsubmitted").addClass("as_submitted");
			Drupal.assignment_studio.functions.load_comments();
			Drupal.assignment_studio.functions.load_assignment();
			Drupal.assignment_studio.functions.load_feedback();
		  },
		});
	  }
	  else {
	    Drupal.assignment_studio.functions.load_comments();
		Drupal.assignment_studio.functions.load_assignment();
		Drupal.assignment_studio.functions.load_feedback();
	  }
	}
	else {
	  if (confirm("Would you like to create an assignment placeholder? (use this if the student was unable to submit their assignment through the system for some reason")) {
		$.ajax({
		  type: "POST",
	      url: Drupal.settings.basePath + "?q=assignment_studio/ajax/create_null/" + Drupal.settings.assignment_studio.active.tid +"/"+ Drupal.settings.assignment_studio.active.uid,
		  success: function(msg) {
			Drupal.settings.assignment_studio.active.nid = msg;
		    $(Drupal.settings.assignment_studio.active.object).attr('id','assignment_'+ Drupal.settings.assignment_studio.active.uid +'_'+ Drupal.settings.assignment_studio.active.tid +'_'+ Drupal.settings.assignment_studio.active.nid);
		  },
		  complete: function(XMLHttpRequest, textStatus) {
		    $(Drupal.settings.assignment_studio.active.object).children(":first").removeClass("as_unsubmitted").addClass("as_submitted");
			Drupal.assignment_studio.functions.load_comments();
			Drupal.assignment_studio.functions.load_assignment();
			Drupal.assignment_studio.functions.load_feedback();
		  },
		});
	  }
	  else {
        //don't display anything cause they said not to create the placeholder
	  }
	}
  });
  $(".as_completed").live('click',function(){
	Drupal.assignment_studio.functions.save_feedback(0);
  });
  $(".as_save").live('click',function(){
	Drupal.assignment_studio.functions.save_feedback(1);
  });
  
  $(".as_saved_feedback").change(function(){
	var cid = $(this).val();
	var confirm_swap = true;
	if (cid != 0) {
	  if ($("#as_feedback_title").val() != '' || $("#as_feedback").val() != '') {
        confirm_swap = confirm("Are you sure you want to replace the feedback entered below?");
	  }
	  if (confirm_swap) {
        $.ajax({
	      type: "POST",
	      url: Drupal.settings.basePath + "?q=assignment_studio/ajax/saved_feedback/" + cid,
		  beforeSend:function (){
		    Drupal.assignment_studio.functions.loading($("#as_feedback_title").prev(),'');
		    Drupal.assignment_studio.functions.loading($("#as_feedback").parent().parent().prev(),'');
		  },
	      success: function(msg){
		    $("#as_feedback_title").val($(".as_saved_feedback option:selected").text());
	        Drupal.assignment_studio.functions.loading($("#as_feedback_title").prev(),'Title');
		    $("#as_feedback").val(msg);
		    Drupal.assignment_studio.functions.loading($("#as_feedback").parent().parent().prev(),'Feedback');
	      },
	    });
	  }
	  else {
		$(this).val(0);  
	  }
	}
  });
  //sync screen up with options set if refresh was hit
  $("#as_view_exemplary").change();
  $("#as_view_grade").change();
  $("#as_view_color").change();
  $("#as_filter_assignment").change();
  $("#as_filter_status").change();
  $("#as_filter_student").change();
  $("#as_filter_group").change();
});