//ELMS: Assignment Studio - Create a View of student data that is managable for large classrooms
//Copyright (C) 2009  The Pennsylvania State University
//
//Bryan Ollendyke
//bto108@psu.edu
//
//Keith D. Bailey
//kdb163@psu.edu
//
//12 Borland
//University Park,  PA 16802
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation; either version 2 of the License,  or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License along
//with this program; if not,  write to the Free Software Foundation,  Inc.,
//51 Franklin Street,  Fifth Floor,  Boston,  MA 02110-1301 USA.

-----------------
Required Modules
-----------------
Core Optional -- Comment and Taxonomy
Interface / Usability -- Prepopulate, Date, Date Pop-up, textimage, jquery_ui

-----------------
Installation
-----------------
  *Download from drupal.org
  *Place in your modules folder of choice and then activate it at admin/build/modules
  *Go to admin/user/permissions to configure who can view the assignment studio (instructor view).  They'll also need to be able to comment on work w/o need for approval in order to function properly
  *Go to admin/content/taxonomy and Create at least one (two prefered) vocabularies.  One for the names of all the assignments (and add some assignment names / placeholders if you like), and another for the classifications of assignments (Blog post, Discussion, Homework, terms like that)
  *Now you're ready to configure the assignment studio Go to admin/settings/assignment_studio .  Here you can set roles to regaurd as students, whether or not to email students, the default type for assignments, what vocabulary stores the assignment list and what vocabulary stores the assignment categories
  *Now click the Course Assignments tab (admin/settings/assignment_studio/course_assignments).  Here you can set your grading structure (under course grading scale) as well as determine points per assignment, what assignment is associated with which grouping, due date, and if the assignment is submitted on the site or not by default (to account for physically handed in work for example).  This table will also help with planning for the course as you can watch the point breakdowns per assignment classification automatically update.  It also allows you to weight certain categories just by setting the assignment point values accordingly.
  *Lastly, go to admin/build/block because you have 3 default blocks that have been generated.  One for the instructor (just with the links I mentioned here), one for the student (currently just a link to their profile), and one that holds links to all the assignments.  Using the prepopulate module we've been able to get students to tag their assignment just by clicking on the correct submit link.  If you go into the block configuration of Assignment Links admin/build/block/configure/assignment_studio/2 you can also alter the way that the link text is presented.  Even if you don't use this block it'll at least give you the correct links to reference for assignment submission so that students don't need to remember to tag their work correctly in order to submit it.
  *You're now ready to start accepting work from students and assess them! Enjoy!
  
-----------------
Known Issues
-----------------
  *Emailing of students doesn't happen yet, everything's in place there's just nothign being sent out at this time
  *Work marked Exemplary doesn't always flag it correctly (visually, it does back end correctly)
  *Exemplary checkbox isn't ticked correctly so submitting work previously marked exemplary will remove the mark
  *Exemplary isn't hooked up with nodequeue or any other module to make it useful at this time
  *Some visual hand offs get missed requiring the user to select the assignment again in order to have the points refreshed correctly (for example).  I've only experienced this a handful of times in the months that I've been working on this
-----------------
Compatability
-----------------
The Assignment Studio should work in the follow browsers:
Camino
Chrome
Firefox 3 (Mac & PC)
Flock (Mac & PC)
IE 8
Netscape
Opera 9.6 (Mac & PC - Minor top bar resize / screen refresh issue)
Safari 4 (Mac & PC)
Seamonkey (Mac & PC)
Webkit

Known to not work in:
Maxthon 2

-----------------
Notes
-----------------
There's still a lot of work to be done in this area and it's functionality will continue to be expanded upon.  Everything has (hopefully) been written in such a way that porting to future versions of Drupal will be easier.

This module's functionality will be vastly increased in scope when used with the Rubricator.  I highly recommend getting it once it comes out, it will probably knock your socks off :)

The Assignment Studio generates 2 tables in the database (realistically it only needs one but for now it's two).  This is because of the rubricator which will piggyback the assignment studio's tables and functionality.  There will be some fields that are just unused so if you look at it at that level this is why.  The Rubricator will also have tables / content types of it's own so just FYI.

All Feedback is very appriciated.  While this product does serve our department first, I hope that it meets the needs of others and that they can help refine our process / product in order to move us all forward.